const path = require('path');
require('dotenv').config({
    path: path.join(__dirname, '.env')
});
var sqlite3 = require("sqlite3");
var sqlite = require("sqlite");
const AWS = require("aws-sdk");
const mime = require('mime-types');
AWS.config = new AWS.Config({
    accessKeyId: process.env.accessKeyId,
    secretAccessKey: process.env.secretAccessKey,
});
const S3Service = new AWS.S3();

async function getAllBucketObjects(continuationToken) {
    var allObjects = [];
    var hasContinuationToken = true;
    while (hasContinuationToken) {
        var bucketData = await getBucketObjects(continuationToken);
        hasContinuationToken = bucketData.NextContinuationToken !== undefined;
        continuationToken = bucketData.NextContinuationToken;
        allObjects.push(bucketData);
    }
    return allObjects;
}

function getBucketObjects(continuationToken = undefined, maxKeys = 1000) {
    return S3Service.listObjectsV2({ Bucket: process.env.bucketName, ContinuationToken: continuationToken, MaxKeys: maxKeys }).promise();
}

async function createSchema(db) {
    await db.run('CREATE TABLE "user" ( "Id" INTEGER NOT NULL, "Username" TEXT NOT NULL, "ApiKey" TEXT NOT NULL, PRIMARY KEY("Id" AUTOINCREMENT) )');
    await db.run('CREATE TABLE "album" ( "Id" INTEGER NOT NULL, "Name" TEXT, PRIMARY KEY("Id" AUTOINCREMENT) )');
    await db.run('CREATE TABLE "image" ( `Key` TEXT NOT NULL UNIQUE, `CreatedDateTime` TEXT NOT NULL, `Size` INTEGER, `OriginalExtension` TEXT NOT NULL, PRIMARY KEY(`Key`) )');
    await db.run('CREATE TABLE "image_album" ( "Id" INTEGER NOT NULL, "AlbumId" INTEGER NOT NULL, "ImageSlug" TEXT NOT NULL, FOREIGN KEY("AlbumId") REFERENCES "album"("Id"), PRIMARY KEY("Id") )');
    await db.run('CREATE TABLE "user_album" ( "Id" INTEGER NOT NULL, "UserId" INTEGER NOT NULL, "AlbumId" INTEGER NOT NULL, FOREIGN KEY("AlbumId") REFERENCES "album"("Id"), FOREIGN KEY("UserId") REFERENCES "user"("Id"), PRIMARY KEY("Id") )');
    await db.run('CREATE INDEX `IX_image_CreatedDateTime_desc` ON `image` ( `CreatedDateTime` DESC )')
}

async function seedSchema(db) {
    await db.run('INSERT INTO user("Id", "Username", "ApiKey") VALUES (1, "fby", "26767018-4b87-4f65-9c9b-e9078e3c986a")');

    var s3Objects = await getAllBucketObjects();
    var dbObject = [];
    for(const e of s3Objects){
        let tmpArr = e.Contents.map((w) => {
            return {
                Key: w.Key,
                CreatedDateTime: w.LastModified.toISOString(),
                Size: w.Size
            }
        });

        await Promise.all(tmpArr.map(async (obj)=>{
            const testo = await S3Service.headObject({
                Bucket: process.env.bucketName,
                Key: obj.Key
            }).promise();

            var mapped = {
                Key: obj.Key,
                CreatedDateTime: testo.LastModified.toISOString(),
                Size: testo.ContentLength,
                OriginalExtension: mime.extension(testo.ContentType)
            };
            dbObject.push(mapped);
        }))
    }
    console.log(`done grabbing metadata, got ${dbObject.length} items`)
    const statement = await db.prepare("INSERT INTO image(Key, CreatedDateTime, Size, OriginalExtension) VALUES (?, ?, ?, ?)")
    dbObject.forEach(async(w) => {
        console.log(w);
        await statement.run(Object.values(w), (err) => {
            console.err(err);
        });

        console.log(w.CreatedDateTime, "added");
    });

    statement.finalize();
}

(async() => {
    var args = process.argv.slice(2);
    var db = await sqlite.open({
        filename: "./storage.db",
        driver: sqlite3.Database
    });

    switch (args[0]) {
        case "seed":
            console.log("seeding schema with prod data...");
            await seedSchema(db);
            break;
        case "create":
            console.log("recreating db...");
            await createSchema(db);
            break;
        default:
            console.log("unknown command");
            break;
    }





    // await createSchema(db);
    // await seedSchema(db);

    db.close();
})()