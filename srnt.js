(async()=>{
    const path = require('path');
    require('dotenv').config({
        path: path.join(__dirname, '.env')
    });
    
    const express = require("express");
    const basicAuth = require('express-basic-auth')
    const fileUpload = require('express-fileupload');
    const AWS = require("aws-sdk");
    const HashId = require("hashids");
    const mime = require('mime-types');
    const sqlite3 = require("sqlite3");
    const sqlite = require("sqlite");
    const exphbs = require('express-handlebars');
    const db = await sqlite.open({
        filename: "./storage.db",
        driver: sqlite3.Database
    });

    var allowedUsers = {};
    await db.each("SELECT Id, Username, ApiKey FROM user", (err, row) => {
        allowedUsers[row.Username] = row.ApiKey;
    })
    
    const handleShutdown = (sig) => {
        console.log(sig + ' signal received.');
        db.close();
        console.log('Closed DB handle. Shutting down...');
        process.exit(0);
    }
    process.on('SIGTERM', handleShutdown);
    process.on('SIGINT', handleShutdown);
    
    let hashId = new HashId('', 6);
    
    AWS.config = new AWS.Config({
        accessKeyId: process.env.accessKeyId,
        secretAccessKey: process.env.secretAccessKey,
    });
    
    const S3Service = new AWS.S3();
    const app = express();
    
    const auth = basicAuth({
        users: allowedUsers,
        challenge: true
    })
    
    const handlebars = exphbs({
        helpers: {
            formatTitle: function(title) {
                if (title) return `${title} – srnt.me`;
                return "srnt.me";
            },
            contains: function(elem, list, options) {
                if(list.indexOf(elem) > -1) {
                  return options.fn(this);
                }
                return options.inverse(this);
            }
        }
    });
    
    app.use(fileUpload());
    app.set('views', path.join(__dirname, 'views'))
    app.engine('handlebars', handlebars);
    app.set('view engine', 'handlebars');
    app.use(express.static(path.join(__dirname, "static")));

    
    app.use("/robots.txt", express.static(__dirname + "/robots.txt"))
    app.use(express.static(__dirname + "/landing"))
    
    app.get("/stats", async function(req, res) {

        const data = await db.get("SELECT COUNT(Key) AS totalPictures, SUM(Size) AS totalBytes FROM image");
        res.send(data);

    });
    
    app.get(["/manage", "/manage/page/:pageNumber"], auth, async(req, res) => {
        const itemsPerPage = 100;
        const statement = await db.prepare(`
            SELECT i.Key as Key, (i.Key || '.' || i.OriginalExtension) as FileName, i.CreatedDateTime, JSON_GROUP_ARRAY(ia.AlbumId) as AlbumIds FROM image AS i
            LEFT JOIN image_album AS ia ON ia.ImageSlug = i.Key
            GROUP BY i.Key
            ORDER BY CreatedDateTime DESC
            LIMIT ? OFFSET ?`);
        const offset = (req.params.pageNumber - 1 || 0) * itemsPerPage;
        const objects = await statement.all([itemsPerPage, offset])
        
        objects.forEach((q)=>{
            q.AlbumIds = JSON.parse(q.AlbumIds);
        }); //🤒🤒🤒🤒🤒🤮🤮🤮🤮🤮

        console.log(objects);

        const albums = await db.all("SELECT Id, Name FROM album");
        albums.forEach(q => {
            q.Id = hashId.encode(q.Id)
        })
        res.render("manage", {
            layout: "manage",
            title: "manage",
            images: objects,
            albums: albums
        });
    });

    app.get("/a/", async (req,res)=>{
        const albums = await db.all("SELECT ia.AlbumId, (ia.ImageSlug || '.' || i.OriginalExtension) as Key FROM image_album as ia JOIN image as i on i.Key = ia.ImageSlug group by albumid");
        albums.forEach(q => {
            q.AlbumId = hashId.encode(q.AlbumId)
        })
        res.render("album_list", {
            title: "all album",
            albums: albums
        });
    });
    
    app.get("/a/:albumSlug", async(req, res) => {
        var albumId = hashId.decode(req.params.albumSlug)[0];
        console.log(albumId);
        const albumImages = await db.all("SELECT ia.AlbumId, (ia.ImageSlug || \".\" || i.OriginalExtension) as Key FROM image_album as ia LEFT JOIN image as i on i.Key = ia.ImageSlug WHERE AlbumId = ?", albumId);

        res.render("album", {
            title: "album",
            images: albumImages
        });

    });
    
    app.post("/a/:albumName", auth, async (req,res) => {
        if(!req.params.albumName){
            res.status(500).end();
        }
        await db.run("INSERT INTO album(Name) VALUES (?)", req.params.albumName)
        var lastId = await db.all("select last_insert_rowid() as albumid");
        console.log(hashId.encode(lastId[0].albumid));
        res.send(req.params.albumName);
    });

    app.post("/a/:albumId/i/:imageId", auth, async (req, res) => {
        console.log(req.params.imageId, req.params.albumId.split(","))
        await db.run("DELETE FROM image_album WHERE ImageSlug = ?", req.params.imageId);
        
        const insertStatement = await db.prepare("INSERT INTO image_album(AlbumId, ImageSlug) VALUES (?, ?)")
        for(const albumId of req.params.albumId.split(",")){
            if(albumId==-1) continue;
            await insertStatement.run(hashId.decode(albumId)[0], req.params.imageId);
        }
        
    });
    
    function getS3ObjectDetails(bucketId, objectId) {
        return new Promise((resolve, reject) => {
            S3Service.headObject({
                Bucket: bucketId,
                Key: objectId
            }, (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        });
    }
    
    function getS3Object(bucketId, objectId) {
        return new Promise((resolve, reject) => {
            S3Service.getObject({
                Bucket: bucketId,
                Key: objectId
            }, (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        });
    }
    
    function legacyFileHandler(req, res) {
        return getS3Object(process.env.bucketName, req.params.imageId)
            .then((data) => {
                res.status(200);
                res.setHeader("Content-Disposition", "inline; filename=" + req.params.imageId)
                res.contentType(data.ContentType);
                res.send(data.Body);
            })
            .catch((err) => {
                console.log("handling err")
                res.status(500);
                res.send({ error: err });
            });
    }
    
    app.get(["/i/:imageId", "/:imageId"], function(req, res) {
        if (req.params.imageId.toString().includes(".")) {
            req.params.fileId = req.params.imageId;
            req.params.imageId = req.params.imageId.split(".")[0]
            return legacyFileHandler(req, res);
        } else {
    
            getS3ObjectDetails(process.env.bucketName, req.params.imageId).then((data) => {
                let fileExtension = mime.extension(data.ContentType);
                if (data.ContentType.includes("image")) {
                    res.render("image", {
                        title: req.params.imageId,
                        imageId: req.params.imageId,
                        fileExtension: fileExtension,
                        url: req.url
                    });
                } else {
                    return legacyFileHandler(req, res);
                }
            }).catch((err) => {
                console.log("handling err")
                res.status(500);
                res.send({ error: err });
            });
        }
    })
    
    function createFileId() {
        return hashId.encode(new Date().getTime());
    }
    
    
    

    app.post(["/i", "/"], auth, async (req, res) => {
    
        let uploadedFile = req.files["file"];
        let fileKey = createFileId();
        let params = {
            Bucket: process.env.bucketName,
            Key: fileKey,
            Body: uploadedFile.data,
            ACL: "public-read",
            ContentType: uploadedFile.mimetype,
            Tagging: `uploader=${req.auth.user}`,
            Metadata: {
                Uploader: req.auth.user
            }
        }
        S3Service.putObject(params, async function(err, uploadres) {
            if(!err){
                const statement = await db.prepare("INSERT INTO image(Key, CreatedDateTime, Size, OriginalExtension) VALUES (?, ?, ?, ?)");
                statement.run([
                    fileKey, 
                    new Date().toISOString(),
                    uploadedFile.data.length,
                    path.extname(uploadedFile.name).replace(".","")
                ]);
            }

            if (!req.accepts("html")) {
                if (err) {
                    res.status(500)
                        .send({ error: err });
                } else {
                    res.status(200)
                        .send({
                        page: fileKey,
                        file: `${fileKey}.${mime.extension(uploadedFile.mimetype)}`
                    });
                }
            } else {
                res.redirect(`${fileKey}`);
            }
    
    
        })
    });
    var users = await db.all("SELECT * FROM user");
    console.log(users);
    
    const port = 9000;
    console.log(`listening on http://localhost:${port}`);
    app.listen(port);
})();